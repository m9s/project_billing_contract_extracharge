# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Project Billing Contract Extracharge',
    'name_de_DE': 'Projektverwaltung Abrechnung Aufschläge',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds the possbility to calculate extra charges on projects
    ''',
    'description_de_DE': '''
    - Fügt die Möglichkeit der Berechnung von Aufschlägen in Projekten hinzu.
    ''',
    'depends': [
        'account_invoice_billing_pricelist_unitprice',
        'account_invoice_pricelist_extracharge',
        'project_billing_contract',
    ],
    'xml': [
        'timesheet.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
