# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelWorkflow, fields
from trytond.pyson import Eval, Not, Equal, If, Bool
from trytond.pool import Pool


class Work(ModelSQL, ModelView):
    _name = 'project.work'

    def __init__(self):
        super(Work, self).__init__()
        self._error_messages.update({
            'no_price_for_date': 'No price found for billing line at date %s',
            })

    def _compute_billing_line_vals(self, line, defaults):
        pricelist_obj = Pool().get('pricelist.pricelist')
        res = []
        billing_vals = pricelist_obj.compute_billing_lines(
                defaults['product'].id, quantity=line.hours,
                from_unit=defaults['from_uom'],
                pricelist=defaults['pricelist'],
                start_time=line.start_time, end_time=line.end_time)
        if not billing_vals:
            self.raise_user_error('no_price_for_date', error_args=(
                    line.start_time.date()))

        for value in billing_vals:
            billing_line = {}
            billing_line['product'] = defaults['product'].id
            billing_line['party'] = defaults['party'].id
            # TODO: which version is better?
            #billing_line['billing_date'] = line.start_time.date()
            billing_line['billing_date'] = value['date']
            billing_line['quantity'] = value['quantity']
            billing_line['unit_price'] = value['price']
            billing_line['unit'] = value['unit']
            billing_line['description'] = value['description'] or defaults['product'].name
            billing_line['contract'] = defaults['contract'] and defaults['contract'].id or False
            res.append(billing_line)
        return res

Work()
