# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Not, Bool, Eval
from trytond.pool import Pool
from trytond.transaction import Transaction


class Line(ModelSQL, ModelView):
    _name = 'timesheet.line'

    start_time = fields.DateTime('Start Time', required=True,
            states={
                'readonly': Not(Bool(Eval('editable')))
            }, depends=['date', 'editable'])
    end_time = fields.DateTime('End Time', required=True,
            states={
                'readonly': Not(Bool(Eval('editable')))
            }, depends=['date', 'editable'])

    def __init__(self):
        super(Line, self).__init__()
        self.hours = copy.copy(self.hours)
        if self.hours.on_change_with is None:
            self.hours.on_change_with = ['start_time', 'end_time']
        if 'start_time' not in self.hours.on_change_with:
            self.hours.on_change_with += ['start_time']
        if 'end_time' not in self.hours.on_change_with:
            self.hours.on_change_with += ['end_time']
        self.hours.states['readonly'] = True

        self.date = copy.copy(self.date)
        if self.date.on_change_with is None:
            self.date.on_change_with = ['start_time']
        if 'start_time' not in self.date.on_change_with:
            self.date.on_change_with += ['start_time']
        if 'start_time' not in self.date.depends:
            self.date.depends = copy.copy(self.date.depends)
            self.date.depends.append('start_time')

        self._reset_columns()

        self._rpc.update({
            'on_change_with_hours': False,
            'on_change_with_date': False,
        })

    def on_change_with_hours(self, vals):
        hours = 0.0
        if vals.get('start_time') and vals.get('end_time'):
            td = vals['end_time'] - vals['start_time']
            td_seconds = (td.microseconds +
                    (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6
            hours = round(float(td_seconds) / float(3600), 2)
        return hours

    def on_change_with_date(self, vals):
        date_obj = Pool().get('ir.date')
        if vals.get('start_time'):
            return vals['start_time'].date()
        return Transaction().context.get('date') or date_obj.today()

    def _check_update_billing_lines(self, vals):
        res = super(Line, self)._check_update_billing_lines(vals)
        for field in ['start_time', 'end_time']:
            if field in vals:
                return True
        return res

Line()
